# -*- coding: utf-8 -*-
from django.db import models
import datetime


class Product(models.Model):
    SIMPLE_STAT = "simple"
    FULL_STAT = "full"
    EXTENDED_STAT = "extended"
    TYPE_OF_STAT = (
        (SIMPLE_STAT, "Упрощенная статистика"),
        (FULL_STAT,  "Полная статистика"),
        (EXTENDED_STAT,  "Расширенная статистика")
    )
    TARIFF = "tariff"
    SERVICE = "service"
    TYPE_OF_PRODUCT = (
        (TARIFF, "Тарифный план"),
        (SERVICE, "Услуга")
    )
    STANDARD_SUPPORT = "standard"
    PERSONAL = "personal"
    PRIORITY = "priority"
    TYPE_OF_SUPPORT = (
        (STANDARD_SUPPORT, "Стандартная техподдержка"),
        (PERSONAL,  "Персональная техподдержка"),
        (PRIORITY, "Приоритетная техподдержка")
    )
    id = models.AutoField(
        primary_key=True
    )
    product_name = models.CharField(
        max_length=100,
        blank=False,
        verbose_name="Тарифный план"
        )
    product_name_latin = models.CharField(
        max_length=100,
        blank=False,
        verbose_name="Тарифный план",
        unique=True,
    )
    type_of_product = models.CharField(
        max_length=100,
        choices=TYPE_OF_PRODUCT,
        default=TARIFF,
        blank=False,
        verbose_name="Тип продукта"
    )
    number_of_cars = models.IntegerField(
        blank=False,
        null=False
    )
    price_1 = models.DecimalField(
        blank=False,
        null=False,
        max_digits=10,
        decimal_places=2,
        default=10.9
    )
    price_6 = models.DecimalField(
        blank=False,
        null=False,
        max_digits=10,
        decimal_places=2,
        default=10.5
    )
    price_12 = models.DecimalField(
        blank=False,
        null=False,
        max_digits=10,
        decimal_places=2,
        default=9.9
    )
    duration = models.IntegerField(
        blank=False,
        null=False
    )
    numbers_of_phone = models.IntegerField(
        blank=False,
        null=False
    )
    numbers_of_link = models.IntegerField(
        blank=False,
        null=False
    )
    stat = models.CharField(
        max_length=100,
        choices=TYPE_OF_STAT,
        default=SIMPLE_STAT,
        blank=False,
        verbose_name="Статистика"
    )
    support = models.CharField(
        max_length=100,
        choices=TYPE_OF_SUPPORT,
        default=STANDARD_SUPPORT,
        blank=False,
        verbose_name="Техподдержка"
    )
    days_prime_show = models.IntegerField(
        blank=True,
        null=True
    )
    inc_rate = models.IntegerField(
        blank=True,
        null=True
    )
    is_promo = models.BooleanField(
        default=True
    )
    is_pers_email = models.BooleanField(
        default=False
    )
    is_pers_domen = models.BooleanField(
        default=False
    )
    is_seo = models.BooleanField(
        default=False
    )

    class Meta(object):
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
        db_table = 'products'

    def __str__(self):
        return str(self.product_name)

    @property
    def description(self):
        return "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}".\
            format(str(self.product_name), str(self.product_name_latin), str(self.number_of_cars),
                   str(self.numbers_of_phone), str(self.numbers_of_link),
                   str(self.stat), str(self.support),
                   str(self.days_prime_show), str(self.inc_rate),
                   str(self.is_promo), str(self.is_pers_email),
                   str(self.is_pers_domen), str(self.is_seo))

    def description_profile(self):
        return "{0}".format( str(self.number_of_cars))

    @staticmethod
    def get_product_by_name(name):
        return Product.objects.filter(product_name_latin=name).get()


class Promo(models.Model):

    class Meta(object):
        verbose_name = "Промо"
        verbose_name_plural = "Промо"
        db_table = 'promos'

    promo_name = models.CharField(
        max_length=100,
        blank=False,
        verbose_name="Название промо"
    )
    promo_title = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Название  промо"
    )
    promo_change = models.CharField(
        max_length=100,
        blank=False,
        verbose_name="Что меняет"
    )
    promo_change_title = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Что меняет русс"
    )
    promo_value = models.IntegerField(
        blank=False,
        null=False
    )
    promo_duration = models.IntegerField(
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(
        default=datetime.datetime.now,
        blank=False,
    )

    @staticmethod
    def get_by_name(name):
        return Promo.objects.filter(promo_name=name).get()
