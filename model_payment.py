# -*- coding: utf-8 -*-
from django.db import models


class Payment(models.Model):
    class Meta(object):
        verbose_name = u"Платеж"
        verbose_name_plural = u"Платежи"
        db_table = 'payments'
        ordering = ['id']

    id = models.AutoField(
        primary_key=True
    )

    user = models.ForeignKey('Users',
        null=False,
        on_delete=models.PROTECT
    )
    order_id_WO = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name="Номер заказа (платежа) WalletOne"
    )
    description_WO = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Назначение платежа"
    )
    description = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Назначение платежа"
    )
    order_id = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Номер заказа наш"
    )
    order_state_WO = models.CharField(
        max_length=10,
        blank=True,
        verbose_name="Состояние оплаты"
    )
    payment_type_WO = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Тип оплаты"
    )
    ext_payment_account_WO = models.CharField(
        max_length=100,
        blank=True,
        verbose_name="Счет, с которого оплата"
    )
    amount_WO = models.DecimalField(
        blank=False,
        null=False,
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    commission_amount_WO = models.DecimalField(
        blank=False,
        null=False,
        max_digits=10,
        decimal_places=2,
        default=0.00
    )
    currency_id_WO = models.IntegerField(
        blank=False,
        null=False,
        default=840
    )
    updated_at_WO = models.DateTimeField(
        null=True,
        verbose_name=u"Дата оплаты")
    created_at_WO = models.DateTimeField(
        null=True,
        verbose_name=u"Дата создания оплаты")
    expired_at_WO = models.DateTimeField(
        null=True,
        verbose_name=u"Срок годности заказа(платежа)")