# -*- coding: utf-8 -*-
from django.contrib.sessions.models import Session
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.conf import settings
from django.db.models import Q
import json
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from automotiverecycler.apps.app1.models import *
from .models import Chat


def filter_for_car_all_possible_test(args):
    brand = args['query'].get('brand')
    model = args['query'].get('model')
    body = args['query'].get('body')
    fuel = args['query'].get('fuel')
    engine = args['query'].get('engine')
    transmission = args['query'].get('transmission')
    year = args['query'].get('year')
    argmap = {}
    if brand:
        argmap['make'] = [Q(model__model_brand__brand_id=brand)]  # We can omit this if 'model' exists
    if model:
        models = [Q(model__model_id=value) for value in model]
        model_item = models.pop()
        model_query = []
        for item in models:
            model_item |= item
        model_query.append(model_item)
        argmap['model'] = model_query
    if body:
        bodytypes = [Q(body__bodytype_id=value) for value in body]
        body_item = bodytypes.pop()
        bodytype_query = []
        for item in bodytypes:
            body_item |= item
        bodytype_query.append(body_item)
        argmap['body'] = bodytype_query
    if fuel:
        fuels = [Q(fuel__fuel_id=value) for value in fuel]
        fuel_item = fuels.pop()
        fuel_query = []
        for item in fuels:
            fuel_item |= item
        fuel_query.append(fuel_item)
        argmap['fuel'] = fuel_query
    if engine:
        engines = [Q(enginecapacity_volume=value) for value in engine]
        engine_item = engines.pop()
        engine_query = []
        for item in engines:
            engine_item |= item
        engine_query.append(engine_item)
        argmap['engine'] = engine_query
    if transmission:
        transmissions = [Q(transmission__transmission_id=value) for value in transmission]
        transmission_item = transmissions.pop()
        transmission_query = []
        for item in transmissions:
            transmission_item |= item
        transmission_query.append(transmission_item)
        argmap['transmission'] = transmission_query
    if year:
        years = [(Q(year_begin__lte=value) & (Q(year_end__gte=value) | Q(year_end=0))) for value in year]
        year_item = years.pop()
        year_query = []
        for item in years:
            year_item |= item
        year_query.append(year_item)
        argmap['year'] = year_query
    return argmap


@csrf_exempt
def get_modification(request):
    sort_col = request.GET.get('order[0][column]', "0")
    sort_dir = request.GET.get('order[0][dir]', 'asc')
    args = json.loads(request.GET['extra'])
    for arg in args['query']:
        try:
            args['query'][arg].remove('undefined')
        except Exception as e:
            print(e)
    limit = int(request.GET.get('length', 10))
    start = int(request.GET.get('start', 0))
    map_sort_dir = {"asc": "", "desc": "-"}
    map_col = {
        "0": ["model__model_title",],
        "4": ["model__model_title", map_sort_dir[sort_dir] + "modification_title__modification_title"],
        "5": ["model__model_title", map_sort_dir[sort_dir] + "body__bodytype_title", map_sort_dir[sort_dir] + "body__bodytype_doors"],
        "6": ["model__model_title", map_sort_dir[sort_dir] + "transmission__transmission_title"],
        "7": ["model__model_title", map_sort_dir[sort_dir] + "fuel__fuel_title"],
        "8": ["model__model_title", map_sort_dir[sort_dir] + "enginecapacity_volume", map_sort_dir[sort_dir] + "enginecapacity_power"],
        "9": ["model__model_title", map_sort_dir[sort_dir] + "year_begin", map_sort_dir[sort_dir] + "year_end"]
    }
    if args['query'] and len(args['interest']) >= 1 and 'models' not in args['interest']:
        argmap = filter_for_car_all_possible_test(args)
        modification = None
        modifications = CarModification.objects
        q = [item for sublist in map(lambda a: a[1], argmap.items()) for item in sublist]

        def optional_q_filter(need_to_exclude, dict_key_to_exclude):
            if need_to_exclude:
                qs = map(lambda a: a[1], filter(lambda a: a[0] != dict_key_to_exclude, argmap.items()))
                q_makes = [item for sublist in qs for item in sublist]
                return modifications.filter(*q_makes)
            return modifications.filter(*q)

        offset = start + limit
        modifications_modifications = optional_q_filter(modification, 'modification')
        num = modifications_modifications.count()
        modifications_modifications = modifications_modifications.order_by(*map_col[sort_col])[start:offset]
        out_list = []
        for m in modifications_modifications:
            b = []
            b.append({m.model.model_id: m.model.model_title})
            b.append({m.modification_title.modification_id: m.modification_title.modification_title})
            b.append( {'doors': m.body.bodytype_doors})
            b.append({m.body.bodytype_id: m.body.bodytype_title})
            b.append({m.transmission.transmission_id: m.transmission.transmission_title})
            b.append({m.fuel.fuel_id: m.fuel.fuel_title})
            b.append(m.enginecapacity_volume)
            b.append( m.enginecapacity_power)
            b.append("%s - %s" % (m.year_begin, m.year_end))
            if m.created_by:
                b.append( m.created_by.user_id)
            else:
                b.append(None)
            b.append(Employer.objects.filter(
                 employer_user__transportunit__unit_carmodification=m.modification_id
            ).count())
            b.append({'mod_id': m.modification_id})
            out_list.append(b)
        return HttpResponse(json.dumps({"recordsTotal": num, "recordsFiltered": num, "data": out_list}))
    else:
        return HttpResponse(json.dumps({"recordsTotal": 0, "recordsFiltered": 0, "data": []}))


@login_required
def chat(request):
    employers_db = Employer.objects.filter(
    ).values(
        'employer_country__name',
        'employer_region__name',
        'employer_city__name',
        'employer_city_name',
        'employer_district',
        'employer_address',
        'employer_id',
        'employer_user__user_id',
        'employer_title',
        'employer_user__user_email',
        'employer_user__user_phone',
        'employer_user__user_name',
        'employer_user__user__first_name',
    ).order_by(
        'employer_country__name',
        'employer_region__name',
        'employer_title'
    ).distinct()

    from collections import defaultdict, OrderedDict
    employers = defaultdict(list)

    def get_num_mess(employer_user_id):
        return Chat.objects.filter(
            chat_user_from=employer_user_id,
            chat_user_to=request.user.id,
            is_read=False
        ).count()

    for result in employers_db:
        result['num_mess'] = get_num_mess(result['employer_user__user_id'])
        if result['employer_country__name'] is not None:
            employers[result['employer_country__name']].append([
                    result['employer_region__name'] if result['employer_region__name'] else 'None',
                    result['employer_city__name'] if result['employer_region__name'] else 'None',
                    result['employer_city_name'] if result['employer_region__name'] else 'None',
                    result['employer_district'] if result['employer_region__name'] else 'None',
                    result['employer_address'] if result['employer_region__name'] else 'None',
                    result['employer_id'],
                    result['employer_user__user_id'],
                    result['employer_title'],
                    result['employer_user__user_email'],
                    result['employer_user__user_phone'],
                    result['employer_user__user_name'],
                    result['employer_user__user__first_name'],
                    result['num_mess']
                ])
        else:
            employers['None'].append([
                result['employer_region__name'] if result['employer_region__name'] else 'None',
                result['employer_city__name'] if result['employer_region__name'] else 'None',
                result['employer_city_name'] if result['employer_region__name'] else 'None',
                result['employer_district'] if result['employer_region__name'] else 'None',
                result['employer_address'] if result['employer_region__name'] else 'None',
                result['employer_id'],
                result['employer_user__user_id'],
                result['employer_title'],
                result['employer_user__user_email'],
                result['employer_user__user_phone'],
                result['employer_user__user_name'],
                result['employer_user__user__first_name'],
                result['num_mess']
            ])
    employers_contact = {}
    for country in employers:
        emp_region = defaultdict(list)
        for region, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12 in employers[country]:
            emp_region[region].append([arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12])
        emp_region_ordered = OrderedDict(sorted(emp_region.items(), key=lambda t: t[0]))
        employers_contact[country] = emp_region_ordered

    for obj in employers_contact:
        employers_contact[obj].default_factory = None
    employers_ordered = OrderedDict(sorted(employers_contact.items(), key=lambda t: t[0]))
    return render(request, "chat.html", {
        "contacts": employers_ordered,
        "socket_url": settings.SOCKET_URL
    })


@csrf_exempt
def node_api(request):
    try:
        session = Session.objects.get(session_key=request.POST.get('session_key'))
        user_id = session.get_decoded().get('_auth_user_id')
        user_to_id = request.POST.get('user_to')
        a = [Chat.objects.create(
            chat_user_from_id=user_id,
            chat_user_to_id=user_to_id,
            text=request.POST.get('text')
        )]
        serialized_chat = serializers.serialize('json', a)
        return HttpResponse(json.dumps({'success': True, 'result': serialized_chat }))
    except Exception as e:
        print(e)
        return HttpResponseServerError(str(e))
